package com.chuuudik.greeter.plain;

import com.chuuudik.greeter.plain.workflow.GreeterWorkflow;
import com.chuuudik.greeter.plain.workflow.GreeterWorkflowImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterMain {

    public static void main(String[] args) {
        GreeterWorkflow workflow = new GreeterWorkflowImpl();
        workflow.greet();
    }

}
