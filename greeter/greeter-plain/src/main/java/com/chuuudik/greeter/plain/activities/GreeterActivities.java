package com.chuuudik.greeter.plain.activities;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public interface GreeterActivities {
    String getName();
    String getGreeting(String name);
    void say(String what);
}
