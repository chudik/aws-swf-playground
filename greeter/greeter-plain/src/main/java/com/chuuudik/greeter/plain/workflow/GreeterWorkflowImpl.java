package com.chuuudik.greeter.plain.workflow;

import com.chuuudik.greeter.plain.activities.GreeterActivities;
import com.chuuudik.greeter.plain.activities.GreeterActivitiesImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorkflowImpl implements GreeterWorkflow {
    private GreeterActivities activities = new GreeterActivitiesImpl();

    public void greet() {
        String name = activities.getName();
        String greeting = activities.getGreeting(name);
        activities.say(greeting);
    }
}
