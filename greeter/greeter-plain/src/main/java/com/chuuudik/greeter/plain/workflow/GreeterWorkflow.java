package com.chuuudik.greeter.plain.workflow;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public interface GreeterWorkflow {
    void greet();
}
