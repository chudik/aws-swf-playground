package com.chuuudik.greeter.swfasync;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;
import com.chuuudik.SWFUtils;
import com.chuuudik.greeter.swfasync.workflow.GreeterWorkflowImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorkflowWorker {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();

        String domain = "greeterTest";
        String taskListToPoll = "greeterListAsync";

        WorkflowWorker workflowWorker = new WorkflowWorker(service, domain, taskListToPoll);
        workflowWorker.addWorkflowImplementationType(GreeterWorkflowImpl.class);
        workflowWorker.start();
    }
}
