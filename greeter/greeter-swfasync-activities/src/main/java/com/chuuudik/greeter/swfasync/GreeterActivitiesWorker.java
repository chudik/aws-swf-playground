package com.chuuudik.greeter.swfasync;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;
import com.chuuudik.SWFUtils;
import com.chuuudik.greeter.swfasync.activities.GreeterActivitiesImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterActivitiesWorker {
    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InstantiationException {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();

        String domain = "greeterTest";
        String taskListToPoll = "greeterListAsync";

        ActivityWorker activityWorker = new ActivityWorker(service, domain, taskListToPoll);
        activityWorker.addActivitiesImplementation(new GreeterActivitiesImpl());
        activityWorker.start();
    }
}
