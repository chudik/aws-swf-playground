package com.chuuudik.greeter.swf;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.chuuudik.SWFUtils;
import com.chuuudik.greeter.swf.workflow.GreeterWorkflowClientExternal;
import com.chuuudik.greeter.swf.workflow.GreeterWorkflowClientExternalFactory;
import com.chuuudik.greeter.swf.workflow.GreeterWorkflowClientExternalFactoryImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterMain {
    public static void main(String[] args) {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();

        String domain = "greeterTest";

        GreeterWorkflowClientExternalFactory factory = new GreeterWorkflowClientExternalFactoryImpl(
                service, domain);
        GreeterWorkflowClientExternal greeter = factory.getClient("testId" + System.currentTimeMillis());
        greeter.greet();
    }
}
