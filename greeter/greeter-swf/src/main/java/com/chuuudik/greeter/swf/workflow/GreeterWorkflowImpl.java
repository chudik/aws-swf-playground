package com.chuuudik.greeter.swf.workflow;

import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.greeter.swf.activities.GreeterActivitiesClient;
import com.chuuudik.greeter.swf.activities.GreeterActivitiesClientImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorkflowImpl implements GreeterWorkflow {
    private GreeterActivitiesClient activities = new GreeterActivitiesClientImpl();

    public void greet() {
        Promise<String> name = activities.getName();
        Promise<String> greeting = activities.getGreeting(name);
        activities.say(greeting);
    }
}
