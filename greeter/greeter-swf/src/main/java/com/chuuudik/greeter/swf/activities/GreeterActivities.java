package com.chuuudik.greeter.swf.activities;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
@ActivityRegistrationOptions(
        defaultTaskScheduleToStartTimeoutSeconds = 300,
        defaultTaskStartToCloseTimeoutSeconds = 10)
@Activities(version = "1.0")
public interface GreeterActivities {
    String getName();
    String getGreeting(String name);
    void say(String what);
}
