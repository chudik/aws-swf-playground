package com.chuuudik.greeter.swfparallel.activities;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterActivitiesImpl implements GreeterActivities {
    @Override
    public String getName() {
        return "Gaia";
    }

    @Override
    public String getGreeting() {
        return "Aloha";
    }

    @Override
    public void say(String greeting, String name) {
        System.out.println(greeting + ", " + name);
    }
}
