package com.chuuudik.greeter.swfparallel;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.chuuudik.SWFUtils;
import com.chuuudik.greeter.swfparallel.workflow.GreeterWorkflowClientExternal;
import com.chuuudik.greeter.swfparallel.workflow.GreeterWorkflowClientExternalFactory;
import com.chuuudik.greeter.swfparallel.workflow.GreeterWorkflowClientExternalFactoryImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
*/
public class GreeterMain {
    public static void main(String[] args) {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();

        String domain = "greeterTest";

        GreeterWorkflowClientExternalFactory factory = new GreeterWorkflowClientExternalFactoryImpl(
                service, domain);
        GreeterWorkflowClientExternal greeter = factory.getClient("testIdAsync" + System.currentTimeMillis());
        greeter.greet();
    }
}
