package com.chuuudik.greeter.swfparallel.workflow;

import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.greeter.swfparallel.activities.GreeterActivitiesClient;
import com.chuuudik.greeter.swfparallel.activities.GreeterActivitiesClientImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorkflowImpl implements GreeterWorkflow {
    private GreeterActivitiesClient activities = new GreeterActivitiesClientImpl();
    @Override
    public void greet() {
        Promise<String> name = activities.getName();
        Promise<String> greeting = activities.getGreeting();
        activities.say(greeting, name);
    }
}
