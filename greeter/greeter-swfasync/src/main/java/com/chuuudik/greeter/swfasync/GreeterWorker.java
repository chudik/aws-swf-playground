package com.chuuudik.greeter.swfasync;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;
import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;
import com.chuuudik.SWFUtils;
import com.chuuudik.greeter.swfasync.activities.GreeterActivitiesImpl;
import com.chuuudik.greeter.swfasync.workflow.GreeterWorkflowImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorker {
    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InstantiationException {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();

        String domain = "greeterTest";
        String taskListToPoll = "greeterListAsync";

        ActivityWorker activityWorker = new ActivityWorker(service, domain, taskListToPoll);
        activityWorker.addActivitiesImplementation(new GreeterActivitiesImpl());
        activityWorker.start();

        WorkflowWorker workflowWorker = new WorkflowWorker(service, domain, taskListToPoll);
        workflowWorker.addWorkflowImplementationType(GreeterWorkflowImpl.class);
        workflowWorker.start();
    }
}
