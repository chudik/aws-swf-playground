package com.chuuudik.greeter.swfasync.activities;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterActivitiesImpl implements GreeterActivities {
    @Override
    public String getName() {
        return "Universe";
    }

    @Override
    public void say(String what) {
        System.out.println(what);
    }
}
