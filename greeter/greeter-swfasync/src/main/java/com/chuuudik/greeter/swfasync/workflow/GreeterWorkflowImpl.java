package com.chuuudik.greeter.swfasync.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.greeter.swfasync.activities.GreeterActivitiesClient;
import com.chuuudik.greeter.swfasync.activities.GreeterActivitiesClientImpl;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class GreeterWorkflowImpl implements GreeterWorkflow {
    private GreeterActivitiesClient activities = new GreeterActivitiesClientImpl();

    @Override
    public void greet() {
        Promise<String> name = activities.getName();
        Promise<String> greeting = getGreeting(name);
        activities.say(greeting);
    }

    @Asynchronous
    private Promise<String> getGreeting(Promise<String> name) {
        String result = "Hello, " + name.get();
        return Promise.asPromise(result);
    }
}
