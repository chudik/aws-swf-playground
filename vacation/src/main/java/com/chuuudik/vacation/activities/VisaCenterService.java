package com.chuuudik.vacation.activities;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
@Activities
@ActivityRegistrationOptions(
        defaultTaskScheduleToStartTimeoutSeconds = 30,
        defaultTaskStartToCloseTimeoutSeconds = 60)
public interface VisaCenterService {

}
