package com.chuuudik.vacation.activities;

import java.util.Date;
import java.util.UUID;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class AirplaneServiceImpl implements AirplaneService {
    @Override
    public String bookTicket(String origin, String destination, Date date) {
        System.out.println("Booking airplane ticket from " + origin
                + " to " + destination + " on " + date);
        return UUID.randomUUID().toString();
    }

    @Override
    public void returnTicket(String ticketId) {
        System.out.println("Return ticket " + ticketId);
    }
}
