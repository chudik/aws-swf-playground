package com.chuuudik.vacation.activities;

import java.util.Date;
import java.util.UUID;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class HotelServiceImpl implements HotelService {
    @Override
    public String bookRoom(String hotelName, Date checkinDate, Date checkoutDate, int numberOfPeople) {
        System.out.println("Booking a room for " + numberOfPeople + " people in "
                + hotelName + " hotel from " + checkinDate + " to "  + checkoutDate);
        return UUID.randomUUID().toString();
    }

    @Override
    public void cancelBooking(String bookingId) {
        System.out.println("Cancelling booking " + bookingId);
    }
}
