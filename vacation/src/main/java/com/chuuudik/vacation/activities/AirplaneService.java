package com.chuuudik.vacation.activities;

import java.util.Date;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
@Activities(version = "1.0")
@ActivityRegistrationOptions(
        defaultTaskScheduleToStartTimeoutSeconds = 30,
        defaultTaskStartToCloseTimeoutSeconds = 60)
public interface AirplaneService {
    String bookTicket(String origin, String destination, Date date);

    void returnTicket(String ticketId);
}
