package com.chuuudik.vacation.activities;

import java.util.Date;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
@Activities
@ActivityRegistrationOptions(
        defaultTaskScheduleToStartTimeoutSeconds = 30,
        defaultTaskStartToCloseTimeoutSeconds = 60)
public interface HotelService {
    String bookRoom(String hotelName, Date checkinDate, Date checkoutDate, int numberOfPeople);

    void cancelBooking(String bookingId);
}
