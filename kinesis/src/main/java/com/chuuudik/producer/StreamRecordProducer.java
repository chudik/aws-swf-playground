package com.chuuudik.producer;

import java.nio.ByteBuffer;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.kinesis.AmazonKinesisClient;
import com.amazonaws.services.kinesis.model.PutRecordRequest;
import com.amazonaws.services.kinesis.model.PutRecordResult;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class StreamRecordProducer {
    private static final String STREAM_NAME = "dev_stream_dnimresu";
    private static final String KINESIS_ENDPOINT = "https://kinesis.eu-central-1.amazonaws.com";

    public static void main(String[] args) {
        AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();
        AmazonKinesisClient kinesisClient = new AmazonKinesisClient(credentialsProvider.getCredentials());
        kinesisClient.setEndpoint(KINESIS_ENDPOINT);

        long creationTime = System.currentTimeMillis();
        String msg = "test" + creationTime;
        PutRecordRequest putRecordRequest = new PutRecordRequest();
        putRecordRequest.setStreamName(STREAM_NAME);
        putRecordRequest.setData(ByteBuffer.wrap(msg.getBytes()));
        putRecordRequest.setPartitionKey("partKey-" + creationTime);

        PutRecordResult putRecordResult = kinesisClient.putRecord(putRecordRequest);
        System.out.println(putRecordResult.toString());
    }
}
