package com.chuuudik.consumer;

import java.nio.ByteBuffer;
import java.util.List;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.IRecordProcessorCheckpointer;
import com.amazonaws.services.kinesis.clientlibrary.types.InitializationInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ProcessRecordsInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownInput;
import com.amazonaws.services.kinesis.clientlibrary.types.ShutdownReason;
import com.amazonaws.services.kinesis.model.Record;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class StreamRecordProcessor implements IRecordProcessor {

    public void initialize(InitializationInput initializationInput) {
        System.out.println("init " + initializationInput.getShardId());
    }

    public void processRecords(ProcessRecordsInput processRecordsInput) {
        System.out.println("process records...");
        for (Record item : processRecordsInput.getRecords()) {
            System.out.println("record " + item.getSequenceNumber() + "  with data " +
                    new String(item.getData().array()));
        }
    }

    public void shutdown(ShutdownInput shutdownInput) {
        System.out.println("shutdown " + shutdownInput.getShutdownReason().name());
    }
}
