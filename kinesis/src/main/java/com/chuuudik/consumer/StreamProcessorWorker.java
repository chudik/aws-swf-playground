package com.chuuudik.consumer;

import java.util.UUID;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.KinesisClientLibConfiguration;
import com.amazonaws.services.kinesis.clientlibrary.lib.worker.Worker;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class StreamProcessorWorker {

    private static final String APPLICATION_NAME = "testStreamProcessorApp";
//    private static final String STREAM_NAME = "dev_stream_dnimresu";
    private static final String STREAM_NAME = "dev_stream_dnimresu-org-orgId";
    private static final String KINESIS_ENDPOINT = "https://kinesis.eu-central-1.amazonaws.com";

    public static void main(String[] args) {
        AWSCredentialsProvider credentialsProvider = new DefaultAWSCredentialsProviderChain();
        String workerId = UUID.randomUUID().toString();
        final KinesisClientLibConfiguration config = new KinesisClientLibConfiguration(APPLICATION_NAME,
                STREAM_NAME, credentialsProvider, workerId);
        config.withKinesisEndpoint(KINESIS_ENDPOINT);
        final IRecordProcessorFactory recordProcessorFactory = new StreamRecordProcessorFactory();
        final Worker worker = new Worker.Builder()
                .recordProcessorFactory(recordProcessorFactory)
                .config(config)
                .build();
        System.out.println("Starting processor worker");
        worker.run();
    }

}
