package com.chuuudik.consumer;

import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessor;
import com.amazonaws.services.kinesis.clientlibrary.interfaces.v2.IRecordProcessorFactory;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class StreamRecordProcessorFactory implements IRecordProcessorFactory {
    public IRecordProcessor createProcessor() {
        return new StreamRecordProcessor();
    }
}
