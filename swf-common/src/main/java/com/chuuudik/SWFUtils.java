package com.chuuudik;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflowClient;

/**
 * Created by mfesenko on 8/10/16.
 */
public class SWFUtils {

    public static AmazonSimpleWorkflow getAmazonSimpleWorkflowService() {
        ClientConfiguration configuration = new ClientConfiguration()
                .withSocketTimeout(70*1000);
        AWSCredentials credentials = getAWSCredentials();
        AmazonSimpleWorkflow service = new AmazonSimpleWorkflowClient(credentials, configuration);
        service.setEndpoint("https://swf.eu-central-1.amazonaws.com");
        return service;
    }

    public static AWSCredentials getAWSCredentials() {
        String accessId = System.getenv("AWS_ACCESS_KEY_ID");
        String secretKey = System.getenv("AWS_SECRET_KEY");
        return new BasicAWSCredentials(accessId, secretKey);
    }

}