package com.chuuudik.parallelization;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.amazonaws.services.simpleworkflow.flow.ActivityWorker;
import com.amazonaws.services.simpleworkflow.flow.WorkflowWorker;
import com.chuuudik.SWFUtils;
import com.chuuudik.parallelization.activities.ParallelActivitiesImpl;
import com.chuuudik.parallelization.workflow.*;

/**
 * Created by mfesenko on 8/10/16.
 */
public class Worker {
    public static void main(String[] args) throws IllegalAccessException, NoSuchMethodException, InstantiationException {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();
        String domain = "parallelizationTest";
        String taskListToPoll = "test";

        ActivityWorker activityWorker = new ActivityWorker(service, domain, taskListToPoll);
        activityWorker.addActivitiesImplementation(new ParallelActivitiesImpl());
        activityWorker.start();

        WorkflowWorker workflowWorker = new WorkflowWorker(service, domain, taskListToPoll);
        workflowWorker.addWorkflowImplementationType(NParallelActivitiesWorkflowImpl.class);
        workflowWorker.addWorkflowImplementationType(NConsequentActivitiesWorkflowImpl.class);
        workflowWorker.addWorkflowImplementationType(MOfNParallelActivitiesWithOrPromiseWorkflowImpl.class);
        workflowWorker.addWorkflowImplementationType(MOfNParallelActivitiesWithExecutionLocalWorkflowImpl.class);
        workflowWorker.addWorkflowImplementationType(MOfNParallelActivitiesWithCounterWorkflowImpl.class);
        workflowWorker.start();
    }
}
