package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.AndPromise;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.activities.ParallelActivitiesClient;
import com.chuuudik.parallelization.activities.ParallelActivitiesClientImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mfesenko on 8/10/16.
 */
public class NParallelActivitiesWorkflowImpl implements NParallelActivitiesWorkflow {
    private ParallelActivitiesClient activitiesClient = new ParallelActivitiesClientImpl();

    @Override
    public void execute(List<ActivityParams> params) {
        List<Promise<Void>> hasRun = new ArrayList<>();
        for (ActivityParams currentParams : params) {
            hasRun.add(activitiesClient.executeActivity(currentParams));
        }
        AndPromise promise = new AndPromise(hasRun.toArray(new Promise[0]));
        processResults(promise);
    }

    @Asynchronous
    public void processResults(Promise<Void> result) {
        System.out.println("Completed " + result.isReady());
    }
}
