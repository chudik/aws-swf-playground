package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.OrPromise;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.activities.ParallelActivitiesClient;
import com.chuuudik.parallelization.activities.ParallelActivitiesClientImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mfesenko on 8/10/16.
 */
public class MOfNParallelActivitiesWithOrPromiseWorkflowImpl implements MOfNParallelActivitiesWithOrPromiseWorkflow {
    private ParallelActivitiesClient client = new ParallelActivitiesClientImpl();

    @Override
    public void execute(List<ActivityParams> params, int maxParallelActivities) {
        OrPromise orPromise = new OrPromise(Promise.Void());
        onActivityCompleted(orPromise, params, maxParallelActivities);
    }

    @Asynchronous
    public void onActivityCompleted(OrPromise orPromise, List<ActivityParams> params, int maxParallelActivities) {
        List<Promise> notReadyPromises = new ArrayList<>();
        for (Promise<?> promise : orPromise.getValues()) {
            if (!promise.isReady()) {
                notReadyPromises.add(promise);
            }
        }
        int countOfActivitiesToStart = maxParallelActivities - notReadyPromises.size();
        for (int i = 0; i < countOfActivitiesToStart; ++i) {
            ActivityParams currentParams = params.get(i);
            notReadyPromises.add(client.executeActivity(currentParams));
        }
        OrPromise newOrPromise = new OrPromise(notReadyPromises.toArray(new Promise[0]));
        onActivityCompleted(newOrPromise, params.subList(countOfActivitiesToStart, params.size()), maxParallelActivities);
    }

}
