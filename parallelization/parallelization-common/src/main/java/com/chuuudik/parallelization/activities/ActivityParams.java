package com.chuuudik.parallelization.activities;

/**
 * Created by mfesenko on 8/19/16.
 */
public class ActivityParams {
    private int id;
    private int minTimeoutSec = 1;
    private int maxTimeoutSec = 6;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMinTimeoutSec() {
        return minTimeoutSec;
    }

    public void setMinTimeoutSec(int minTimeoutSec) {
        this.minTimeoutSec = minTimeoutSec;
    }

    public int getMaxTimeoutSec() {
        return maxTimeoutSec;
    }

    public void setMaxTimeoutSec(int maxTimeoutSec) {
        this.maxTimeoutSec = maxTimeoutSec;
    }
}
