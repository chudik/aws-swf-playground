package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.activities.ParallelActivitiesClient;
import com.chuuudik.parallelization.activities.ParallelActivitiesClientImpl;

import java.util.List;

/**
 * Created by mfesenko on 8/10/16.
 */
public class NConsequentActivitiesWorkflowImpl implements NConsequentActivitiesWorkflow {
    private ParallelActivitiesClient client = new ParallelActivitiesClientImpl();

    @Override
    public void execute(List<ActivityParams> params) {
        runActivity(params, 0);
    }


    @Asynchronous
    public void runActivity(List<ActivityParams> params, int i, Promise<Void> ... promises) {
        if (i < params.size()) {
            Promise<Void> hasRun = client.executeActivity(params.get(i));
            runActivity(params, ++i, hasRun);
        } else {
            System.out.println("completed");
        }
    }
}
