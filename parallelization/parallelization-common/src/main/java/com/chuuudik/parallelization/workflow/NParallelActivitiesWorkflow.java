package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Execute;
import com.amazonaws.services.simpleworkflow.flow.annotations.Workflow;
import com.amazonaws.services.simpleworkflow.flow.annotations.WorkflowRegistrationOptions;
import com.chuuudik.parallelization.activities.ActivityParams;

import java.util.List;

/**
 * Created by mfesenko on 8/10/16.
 */
@Workflow
@WorkflowRegistrationOptions(defaultExecutionStartToCloseTimeoutSeconds = 300)
public interface NParallelActivitiesWorkflow {
    @Execute(version = "1.0")
    void execute(List<ActivityParams> params);
}
