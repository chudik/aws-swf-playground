package com.chuuudik.parallelization;

import com.amazonaws.services.simpleworkflow.AmazonSimpleWorkflow;
import com.chuuudik.SWFUtils;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.workflow.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
public class Main {
    public static void main(String[] args) {
        AmazonSimpleWorkflow service = SWFUtils.getAmazonSimpleWorkflowService();
        String domain = "parallelizationTest";

        List<ActivityParams> params = new ArrayList<ActivityParams>();
        for (int i = 0; i < 40; ++i) {
            ActivityParams currentParams = new ActivityParams();
            currentParams.setId(i);
            currentParams.setMinTimeoutSec(15);
            currentParams.setMaxTimeoutSec(20);
            params.add(currentParams);
        }
//        NParallelActivitiesWorkflowClientExternalFactory factory = new NParallelActivitiesWorkflowClientExternalFactoryImpl(
//                service, domain);
//        NConsequentActivitiesWorkflowClientExternalFactory factory = new NConsequentActivitiesWorkflowClientExternalFactoryImpl(
//                service, domain);
//        MOfNParallelActivitiesWithOrPromiseWorkflowClientExternalFactory factory = new MOfNParallelActivitiesWithOrPromiseWorkflowClientExternalFactoryImpl(
//                service, domain);
//        MOfNParallelActivitiesWithExecutionLocalWorkflowClientExternalFactory factory = new MOfNParallelActivitiesWithExecutionLocalWorkflowClientExternalFactoryImpl(
//                service, domain);
        MOfNParallelActivitiesWithCounterWorkflowClientExternalFactory factory = new MOfNParallelActivitiesWithCounterWorkflowClientExternalFactoryImpl(
                service, domain);
//        factory.getClient("testId" + System.currentTimeMillis()).execute(params);
        factory.getClient("testId" + System.currentTimeMillis()).execute(params, 5);
    }
}
