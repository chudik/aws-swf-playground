package com.chuuudik.parallelization.activities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by mfesenko on 8/10/16.
 */
public class ParallelActivitiesImpl implements ParallelActivities {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    public void executeActivity(ActivityParams params) {
        logMessageToSystemOut("starting activity " + params.getId());
        Random random = new Random(System.currentTimeMillis());
        int timeoutSeconds = params.getMinTimeoutSec() + random.nextInt(params.getMaxTimeoutSec() - params.getMinTimeoutSec());
        try {
            Thread.sleep(timeoutSeconds * 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        logMessageToSystemOut("stopped activity " + params.getId() + " after sleeping for " + timeoutSeconds + "sec");
    }

    private void logMessageToSystemOut(String message) {
        System.out.println(dateFormat.format(new Date()) + ": " + message);
    }
}
