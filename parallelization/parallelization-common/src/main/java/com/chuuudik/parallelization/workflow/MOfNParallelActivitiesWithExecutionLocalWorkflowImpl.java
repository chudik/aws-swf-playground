package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.WorkflowExecutionLocal;
import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.activities.ParallelActivitiesClient;
import com.chuuudik.parallelization.activities.ParallelActivitiesClientImpl;

import java.util.List;

/**
 * Created by mfesenko on 8/22/16.
 */
public class MOfNParallelActivitiesWithExecutionLocalWorkflowImpl implements MOfNParallelActivitiesWithExecutionLocalWorkflow {
    private ParallelActivitiesClient client = new ParallelActivitiesClientImpl();
    private WorkflowExecutionLocal<WorkflowState> workflowState = new WorkflowExecutionLocal<>();

    @Override
    public void execute(List<ActivityParams> params, int maxParallelActivities) {
        start(params,maxParallelActivities);
    }

    public void start(List<ActivityParams> params, int maxParallelActivities) {
        WorkflowState state = getWorkflowState();
        while (state.getStartedActivitiesCount() < params.size()
                && state.getRunningActivitiesCount() < maxParallelActivities) {
            ActivityParams currentParams = params.get(state.getStartedActivitiesCount());
            state.incrementCounters();

            Promise<Void> promise = client.executeActivity(currentParams);
            onCompleted(promise, maxParallelActivities, params);
        }
    }

    @Asynchronous
    public void onCompleted(Promise<Void> promise, int maxParallelActivities, List<ActivityParams> params) {
        getWorkflowState().decrementRunningActivitiesCount();
        start(params,maxParallelActivities);
    }

    private WorkflowState getWorkflowState() {
        if (workflowState.get() == null) {
            workflowState.set(new WorkflowState());
        }
        return workflowState.get();
    }

    private static final class WorkflowState {
        private int startedActivitiesCount;
        private int runningActivitiesCount;

        public int getStartedActivitiesCount() {
            return startedActivitiesCount;
        }

        public int getRunningActivitiesCount() {
            return runningActivitiesCount;
        }

        public void incrementCounters() {
            ++this.startedActivitiesCount;
            ++this.runningActivitiesCount;
        }

        public void decrementRunningActivitiesCount() {
            --this.runningActivitiesCount;
        }
    }
}
