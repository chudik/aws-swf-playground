package com.chuuudik.parallelization.workflow;

import com.amazonaws.services.simpleworkflow.flow.annotations.Asynchronous;
import com.amazonaws.services.simpleworkflow.flow.core.Promise;
import com.chuuudik.parallelization.activities.ActivityParams;
import com.chuuudik.parallelization.activities.ParallelActivitiesClient;
import com.chuuudik.parallelization.activities.ParallelActivitiesClientImpl;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by mfesenko on 8/23/16.
 */
public class MOfNParallelActivitiesWithCounterWorkflowImpl implements MOfNParallelActivitiesWithCounterWorkflow {
    private ParallelActivitiesClient client = new ParallelActivitiesClientImpl();
    private int runningActivities = 0;

    @Override
    public void execute(List<ActivityParams> params, int maxParallelActivities) {
        LinkedList<ActivityParams> paramsList = new LinkedList<>();
        paramsList.addAll(params);
        start(paramsList, maxParallelActivities);
    }

    public void start(LinkedList<ActivityParams> params, int maxParallelActivities) {
        while (runningActivities < maxParallelActivities && !params.isEmpty()) {
            ActivityParams currentParams = params.removeFirst();
            ++runningActivities;
            Promise<Void> promise = client.executeActivity(currentParams);
            onCompleted(promise, maxParallelActivities, params);
        }
    }

    @Asynchronous
    public void onCompleted(Promise<Void> promise, int maxParallelActivities,
                            LinkedList<ActivityParams> params) {
        --runningActivities;
        start(params, maxParallelActivities);
    }
}
