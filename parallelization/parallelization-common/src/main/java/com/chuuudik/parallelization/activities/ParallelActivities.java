package com.chuuudik.parallelization.activities;

import com.amazonaws.services.simpleworkflow.flow.annotations.Activities;
import com.amazonaws.services.simpleworkflow.flow.annotations.ActivityRegistrationOptions;

/**
 * @author Mary Fesenko <m.fesenko17@gmail.com>
 */
@Activities(version = "1.0")
@ActivityRegistrationOptions(
        defaultTaskScheduleToStartTimeoutSeconds = 30,
        defaultTaskStartToCloseTimeoutSeconds = 300)
public interface ParallelActivities {
    void executeActivity(ActivityParams params);
}
